#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <TimerEvent.h>
#include <ArduinoOTA.h>
#include <WebSerial.h>
#include "arduino_secrets.h"

/*
  MQTT Rain sensor

  A simple rain sensur using a analog rain sensor at a Wemos D1
  Rename arduino_secrets.h.example in arduino_secrets.h and fill in your own credentials

  The circuit:
  * A0 rain sensor

  Created 25-07-2021
  By Tom van der Meer
  Modified 17-08-2021
  By Tom van der Meer

  https://gitlab.com/tommeer/arduino/mqtt-rain

*/

// Constants
const char* ssid = SECRET_WIFISSID;
const char* password =  SECRET_WIFIPASS; // Enter WiFi password
const char* mqttServer = SECRET_SERVER;
const int   mqttPort = SECRET_PORT;
const char* mqttUser = SECRET_MQTTUSER;
const char* mqttPassword = SECRET_MQTTPASS;

const long utcOffsetInSeconds = 3600;
const int updateInterval = 30e3;

// IO
const int diSleepEnbl = D7;
const int doSensEnbl = D8;
const int aiSensValue = A0;

// Variables
char buf[4]; // Buffer to store the sensor value
int loopCnt = 0;
int newVal = 0;
//bool b = false;
 
// Define MQTT client
WiFiClient espClient;
PubSubClient client(espClient);

// Define TimerEvent for reading the sensor in intervals
TimerEvent timer;

// Define WebSerial server for remote debugging
AsyncWebServer server(80);

void recvMsg(uint8_t *data, size_t len){
  WebSerial.println("Received Data...");
  String d = "";
  for(int i=0; i < len; i++){
    d += char(data[i]);
  }
  WebSerial.println(d);
}

void ClientCallback(char* topic, byte* payload, unsigned int length) {
 
  Serial.print("Message arrived in topic: ");
  Serial.println(topic);
 
  Serial.print("Message:");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
 
  Serial.println();
  Serial.println("-----------------------");
 
}

int Cnv_Analog_Pct(int val){
  // convert analogread [0-1024] to percent [0-100]
  return 100-round(val / 10.24);
}

void UpdateSensor(){
  int val = 0;
  digitalWrite(LED_BUILTIN,LOW);
  digitalWrite(doSensEnbl,HIGH);
  delay(100);
  val = Cnv_Analog_Pct(analogRead(aiSensValue));
  digitalWrite(doSensEnbl,LOW);

  Serial.print("New sensor value: ");
  Serial.println(val);  
  WebSerial.println("New sensor value: "+String(val));
  
  client.publish("home-assistant/esp/rain", itoa(val, buf, 10));
  digitalWrite(LED_BUILTIN,HIGH);
}

void setup() {
  // Config IO -----------------------
  pinMode(diSleepEnbl, INPUT);
  pinMode(doSensEnbl, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);

  // Config Serial -----------------------
  Serial.begin(115200);
  delay(200);
  Serial.println("");

  // Connect WiFi -----------------------
  Serial.print("Connecting to "+String(ssid)+":");
  //WiFi.mode(WIFI_STA); 
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) 
  {
    digitalWrite(LED_BUILTIN,HIGH);
    delay(200);
    digitalWrite(LED_BUILTIN,LOW);
    delay(800);
    Serial.print(WiFi.status());
  }
  Serial.println(":Connected");
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());
  
  // Enable auto light sleep
  wifi_set_sleep_type(LIGHT_SLEEP_T);
   
  client.setServer(mqttServer, mqttPort);
  client.setCallback(ClientCallback);

  int attempts = 0;
  while (!client.connected()) {
    attempts++;
    Serial.print("Connecting to MQTT...");
    if (client.connect("ESP8266Client", mqttUser, mqttPassword )) {
      Serial.println("Connected");  
    } else {        
      Serial.print("MQTT connect failed with state ");
      Serial.println(client.state());

      for (int i = 0; i < client.state(); i++) {
        digitalWrite(LED_BUILTIN,LOW);
        delay(200);
        digitalWrite(LED_BUILTIN,HIGH);
        delay(200);
      }
      delay(3000);
      ESP.restart();
    }
  }
  client.subscribe("home-assistant/esp/rain");

  timer.set(updateInterval, UpdateSensor);

  WebSerial.begin(&server);
  WebSerial.msgCallback(recvMsg);
  server.begin();
    
  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
   ArduinoOTA.setHostname("MQTT-Rain");

  // No authentication by default
  // ArduinoOTA.setPassword((const char *)"123");

  ArduinoOTA.onStart([]() {
    Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
}
 
void loop() {
  client.loop();
  timer.update();
  ArduinoOTA.handle();

  delay(1000);
}
