# MQTT Rain sensor

Simple sensor that sends rain information over MQTT

## Description

A simple rain sensur using a analog rain sensor and a Wemos D1.
I use this sensor to check for rain to quickly respond my sun screen.
From v0.2 i try to use it also to see how much rainfall has fallen to use
it for my garden watering system.

## Getting Started

Rename arduino_secrets.h.example in arduino_secrets.h and fill in your own credentials

### Dependencies

The project uses the following libraries:
* ESP8266WiFi - https://github.com/esp8266/Arduino/tree/master/libraries/ESP8266WiFi
* PubSubClient - https://github.com/knolleary/pubsubclient
* v0.1
  * EEPROM - https://www.arduino.cc/en/Reference/EEPROM
* v0.2
  * TimerEvent - https://github.com/cygig/TimerEvent
  * ArduinoOTA - https://github.com/jandrassy/ArduinoOTA
  * WebSerial - https://github.com/ayushsharma82/WebSerial

### The circuit

* A0 rain sensor
* D8 power to sensor to save more energy
* v0.1 only
  * D7 to enable deep sleep (or disable if you want to upload new sketch)

## Authors

Tom van der Meer
[@TomMeer](https://gitlab.com/tommeer/)

## Version History

* 0.1
    * Initial Release, super lightweight with Deep Sleep

* 0.2
    * Fixed update interval (to be able to use statistics in Home Assistant), OTA and WebSerial support for better remote diagnostics and updates.

## License

This project is licensed under the MIT License - see the LICENSE.md file for details